package cn.rock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ServiceCenterDemoApplication {
    private static Logger logger = LoggerFactory.getLogger ( ServiceCenterDemoApplication.class );

    public static void main(String[] args) {
        logger.info ( "程序启动-------------------------------------------------" );
        SpringApplication.run ( ServiceCenterDemoApplication.class, args );
    }
}
